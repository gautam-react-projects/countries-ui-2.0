import React, { useState } from "react";
import "./FilterSubRegionBar.css"
export default function FilterSubRegion(props) {
  function handleClick(event) {
    const SUBREGION = event.target.value;
    const Region = props.currentRegion;
    props.updatestate(SUBREGION);
  }

  const Data = props.data;

  const subregions = props.options;

  return (
    <>
      <div className="dropdownMenu">
        <select onChange={handleClick}>
          <option value="" selected>
            {props.default}
          </option>
          {subregions.map((obj) => (
            <option value={obj}>{obj}</option>
          ))}
        </select>
      </div>
    </>
  );
}
