import React from "react";
import "./SearchBar.css"
export default function SearchBar(props) {
  const handleChange = (event) => {
    let SearchedCountry = event.target.value;

    props.updatestate(SearchedCountry);
  };

  return (
    <div className="searchBar">
      <input
        className="searchCountry"
        placeholder=" Search for a Country.."
        onChange={handleChange}
        name="country"
      ></input>
    </div>
  );
}
