import React from 'react'
import "./SortBar.css"

export default function SortBar(props) {

    function handleClick(event) {
        const sorting = event.target.value;
    
        props.updatestate(sorting);
       
      }

    const sortings = props.options;

    const key = props.keys;
  return (
    <div className="dropdownMenu">
        <select onChange={handleClick}>
          <option value="" selected>
            {props.default}
          </option>
          {sortings.map((obj , index) => (
            <option value={key[index]}>{obj}</option>
          ))}
        </select>
      </div>
  )
}
