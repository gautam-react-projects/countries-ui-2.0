import React, { useState, useEffect } from "react";
import CountryCard from "../CountryCard/CountryCard";
import MainBar from "../MainBar/MainBar";

export default function ConutriesAPI() {
  return fetch("https://restcountries.com/v3.1/all").then((Response) =>
    Response.json()
  );
}
