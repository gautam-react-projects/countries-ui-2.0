import React, { useState } from "react";

export default function FilterMenu(props) {
  const Data = props.data;

  function handleClick(event) {
    const SelectedRegion = event.target.value;

    props.updatestate(SelectedRegion);
  }

  const options = props.options;

  return (
    <div className="dropdownMenu">
      <select onChange={handleClick}>
        <option value="" selected>
          {props.default}
        </option>
        {options.map((obj) => (
          <option value={obj}>{obj}</option>
        ))}
      </select>
    </div>
  );
}
