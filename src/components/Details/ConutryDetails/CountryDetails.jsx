import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { Link } from "react-router-dom";
import "./CountryDetails.css";

export default function CountryDetails() {
  const [countryData, setcountryData] = useState("");

  const { CountryCapital } = useParams();

  useEffect(() => {
    fetch(`https://restcountries.com/v3.1/capital/${CountryCapital}`)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setcountryData(data);
      });
  }, []);
  return (
    <>
      {countryData !== "" ? (
        <div className="countryDetails">
          <div className="navBar">
            <h2>Where in the world? </h2>
          </div>
          <div className="mainContainer">
            <div className="backButtondiv">
              <Link to="/">
                <button className="backButton"> ← Back</button>
              </Link>
            </div>
            <div className="aboutCountry">
              <img
                className="countryFlagImage"
                src={countryData[0].flags.png}
                alt="flag"
              />

              <div className="allInformation">
                <div className="info">
                  <h1>{countryData[0].name.common}</h1>

                  <div className="informationlayer1">
                    <div className="leftInfo">
                      <h5>
                        Native Name :
                        {countryData[0].name.nativeName?.[
                          Object.keys(countryData[0].name.nativeName)[0]
                        ].common || "N/A"}
                      </h5>
                      <h5>Population :{countryData[0].population}</h5>
                      <h5>Region:{countryData[0].region}</h5>
                      <h5>Sub Region:{countryData[0].subregion}</h5>
                      <h5>Capital:{countryData[0].capital}</h5>
                    </div>
                    <div className="rightInfo">
                      <h5>Total Level domain: be</h5>

                      <h5>Languages: loading</h5>
                      <h5>
                        Currencies:{" "}
                        {Object.values(countryData[0].currencies)?.[0]?.name ||
                          "NA"}
                      </h5>

                      <h5>
                        Languages:{" "}
                        {Object.values(countryData[0].languages).map(
                          (language) => language + " "
                        )}{" "}
                      </h5>
                    </div>
                  </div>
                </div>

                <div className="borderCountries">
                  <h5>Border Countries: </h5>
                  {countryData[0].borders?.length ? (
                    countryData[0].borders.map((country) => (
                      <h5>| {country} | </h5>
                    ))
                  ) : (
                    <> NA </>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : (
        <>
          <h4> Loading !!!!!!</h4>
          <h2> {CountryCapital} details</h2>
        </>
      )}
    </>
  );
}
