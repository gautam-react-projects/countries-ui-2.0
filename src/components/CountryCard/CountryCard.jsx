import React from "react";
import "./CountryCard.css";
import { Link } from "react-router-dom";
export default function CountryCard(props) {
  const { official } = props.data.name;
  const { population, region, capital, subregion, area } = props.data;
  const { png } = props.data.flags;
  return (
    <div className="card">
      <Link to={`${capital}`} style={{ textDecoration: "none" }}>
        <img
          className="flagImage"
          src={png}
          style={{ textDecoration: "none" }}
        />
        <h4><b>Country :{official}</b></h4>
        <h4>Population :{population} </h4>
        <h4>Capital: {capital} </h4>
        <h4>Region :{region}</h4>
        <h4>Sub-region:{subregion}</h4>
        <h4>Area : {area}</h4>
      </Link>
    </div>
  );
}
