import React, { useState, useEffect } from "react";
import CountryCard from "../CountryCard/CountryCard";
import SearchBar from "../SearchBar/SearchBar";
import SortBar from "../SortBar/SortBar";
// import Data from "../countries.json";
import "./MainBar.css";
import FilterRegionBar from "../FilterRegionsBar/FilterRegionsBar.jsx";
import FilterSubRegionBar from "../FilterSubRegionBar/FilterSubRegionBar";
import CountriesAPI from "../Services/CountriesAPI";
export default function Main() {
  const [CountriesData, setCountriesData] = useState([]);
  let finalData = [];
  let flag = false;
  useEffect(() => {
    CountriesAPI().then((response) => {
      flag = true;
      setCountriesData(response);
    });
  }, []);

  // States
  const [Sorting, setSorting] = useState("");
  const [Region, setRegion] = useState("");
  const [SubRegion, setSubRegion] = useState("");
  const [SearchCountry, setSearchCountry] = useState("");

  // state updater functions

  function updateRegion(NewRegion) {
    setRegion(NewRegion);
  }

  function updateSubRegion(subRegion) {
    setSubRegion(subRegion);
  }

  function updateSearchCountry(Country) {
    setSearchCountry(Country);
  }

  function updateSorting(sorting) {
    setSorting(sorting);
  }

  // custom sorting function
  function comparator(a, b) {
    if (Sorting === "official") {
      if (a.name.official < b.name.official) {
        return -1;
      } else if (a.name.official > b.name.official) {
        return 1;
      } else {
        return 0;
      }
    } else if (Sorting === "area") {
      if (a.area < b.area) {
        return 1;
      } else if (a.area > b.area) {
        return -1;
      } else {
        return 0;
      }
    } else if (Sorting === "population") {
      if (a.population < b.population) {
        return 1;
      } else if (a.population > b.population) {
        return -1;
      } else {
        return 0;
      }
    }
  }

  // Data Handlers

  const allRegions = CountriesData.reduce((accumulator, current) => {
    if (!accumulator.includes(current.region)) {
      accumulator.push(current.region);
    }
    return accumulator;
  }, []);

  const allSubRegions = CountriesData.reduce((accumulator, current) => {
    if (Region !== "") {
      if (
        !accumulator.includes(current.subregion) &&
        current.subregion !== undefined &&
        current.region === Region
      ) {
        accumulator.push(current.subregion);
      }
    } else if (
      !accumulator.includes(current.subregion) &&
      current.subregion !== undefined
    ) {
      accumulator.push(current.subregion);
    }

    return accumulator;
  }, []);

  const SortingOptions = ["Name", "Area", "Population"];

  const Sortingkeys = ["official", "area", "population"];

  let ProcessedData = CountriesData;

  // Searching operation

  if (SearchCountry !== "") {
    ProcessedData = ProcessedData.filter((obj) => {
      let uppercaseCountry = obj.name.official.toUpperCase();

      let lowercaseCountry = obj.name.official.toLowerCase();

      if (
        uppercaseCountry.includes(SearchCountry) ||
        lowercaseCountry.includes(SearchCountry)
      ) {
        return obj;
      }
    });
  }

  // Region filter
  if (Region !== "") {
    ProcessedData = ProcessedData.filter((obj) => {
      if (obj.region === Region) {
        return obj;
      }
    });
  }

  // Sub-Region Filter
  if (SubRegion != "") {
    ProcessedData = ProcessedData.filter((obj) => {
      if (obj.subregion === SubRegion) {
        return obj;
      }
    });
  }

  // sorting logics

  if (Sorting !== "") {
    ProcessedData.sort(comparator);
  }

  if (ProcessedData.length <= 0) {
    if (CountriesData.length <= 0) {
      finalData = <h1 className="noresultmessage">Loading !!!</h1>;
    } else {
      finalData = <h1 className="noresultmessage"> No results found !!!</h1>;
    }
  } else {
    finalData = ProcessedData.map((obj) => {
      return <CountryCard data={obj} />;
    });
  }

  return (
    <>
      <div className="main">
        <>
          <SearchBar data={CountriesData} updatestate={updateSearchCountry} />
          <FilterRegionBar
            options={allRegions}
            default={"Filter by Region"}
            data={CountriesData}
            updatestate={updateRegion}
          />
        </>
        <>
          <FilterSubRegionBar
            options={allSubRegions}
            default={"Filter by SubRegion"}
            updatestate={updateSubRegion}
            data={CountriesData}
          />
        </>
        <>
          <SortBar
            options={SortingOptions}
            keys={Sortingkeys}
            default={"Sort by"}
            updatestate={updateSorting}
          />
        </>
      </div>
      <div className="displayWindow">{finalData}</div>
    </>
  );
}
