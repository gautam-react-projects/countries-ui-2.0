import { useState } from "react";
import "./App.css";
import MainBar from "../src/components/MainBar/MainBar";
import FilterRegionBar from "../src/components/FilterRegionsBar/FilterRegionsBar";
import FilterSubRegionBar from "../src/components/FilterSubRegionBar/FilterSubRegionBar";
import Navbar from "./components/NavBar/NavBar";
import CountriesAPI from "./components/Services/CountriesAPI";
import CountryDetails from "./components/Details/ConutryDetails/CountryDetails";
import { Route, BrowserRouter as Router, Routes } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route
            exact
            path="/"
            element={
              <>
                <Navbar />
                <MainBar />
              </>
            }
          />

          <Route exact path="/:CountryCapital" element={<CountryDetails />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
